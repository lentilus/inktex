#!/bin/bash

# source "$ZETTEL_WORKDIR/globals"

log() {
	script_name=${0##*/}
	timestamp=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
	echo "== $script_name $timestamp" "$@" >> "$LOG_FILE"
}

parse_args() {
    OPTSTRING=":n:t:p:"

    while getopts ${OPTSTRING} opt; do
        #
        # we never have spaces in zettels, tags or referneces
        # to avoid bugs, so lets just remove them here
        #
        OPTARG=${OPTARG// /_}
        case ${opt} in
            n)
                new_title=$OPTARG
                ;;
            t)
                title=$OPTARG
                ;;
            p)
                path=$OPTARG
                ;;
            :)
                log "missing args $OPTARG"; exit
                ;;
            ?)
                log "invalid option: $OPTARG"; exit
                ;;
        esac
    done

    echo "title='$title';\
          new_title='$new_title';
          path='$path';"
}

